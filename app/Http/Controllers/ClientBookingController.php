<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Client;
use Illuminate\Database\Eloquent\Collection;

class ClientBookingController extends Controller
{
    /**
     * Get client's bookings.
     *
     * @param Client $client
     *
     * @return Collection
     */
    public function index(Client $client): Collection
    {
        return $client->bookings;
    }

    /**
     * Get a booking.
     *
     * @param Booking $booking
     *
     * @return Booking
     */
    public function show(Booking $booking): Booking
    {
        return $booking;
    }

    /**
     * Create a booking.
     *
     * @param Request $request
     * @param Client $client
     *
     * @return JsonResponse
     */
    public function store(Request $request, Client $client): JsonResponse
    {
        $booking = new Booking($this->getValidatedData($request));
        $client->bookings()->save($booking);

        return response()->json($booking, 201);
    }

    /**
     * Get validated data.
     *
     * @param Request $request
     *
     * @return array
     */
    private function getValidatedData(Request $request): array
    {
        return $request->validate([
            'price' => [
                ...(!$request->isMethod('PATCH') ? ['required'] : []),
                'max:255|numeric',
            ],
            'check_in_date' => [
                ...(!$request->isMethod('PATCH') ? ['required'] : []),
                'gte:1|date|before_or_equal:check_out_date',
            ],
            'check_out_date' => [
                ...(!$request->isMethod('PATCH') ? ['required'] : []),
                'gte:1|date|after_or_equal:check_in_date',
            ],
        ]);
    }
}
