<?php

namespace Tests\Feature;

use App\Models\Booking;
use App\Models\Client;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\AbstractDatabaseTestCase;
use Generator;
use DateTime;

class ClientBookingTest extends AbstractDatabaseTestCase
{
    /**
     * Test client bookings list.
     */
    public function test_returns_a_list_of_client_bookings(): void
    {
        $client = Booking::first()->client;
        $response = $this->getJson("/api/clients/$client->id/bookings");

        $response->assertOk();
        $response->assertJson(fn(AssertableJson $json) => $json->has(Booking::whereBelongsTo($client)->count())
            ->each(fn(AssertableJson $json) => $json->whereAllType([
                'id' => 'integer',
                'price' => ['double', 'integer'],
                'check_in_date' => 'string',
                'check_out_date' => 'string',
                'client_id' => 'integer',
                'created_at' => 'string',
                'updated_at' => 'string',
            ]))
        );
    }

    /**
     * Test creating a client's booking successfully.
     *
     * @dataProvider bookingDataProvider
     *
     * @param string $name
     * @param int $age
     */
    public function test_creating_a_clients_booking_successfully(float $price, DateTime $check_in_date, DateTime $check_out_date): void
    {
        $client = Client::first();
        $nBookings = Booking::count();
        $response = $this->postJson(
            "/api/clients/$client->id/bookings",
            [
                'price' => $price, 
                'check_in_date' => $check_in_date, 
                'check_out_date' => $check_out_date
            ]
        );

        $response->assertCreated();
        $response->assertJson(fn(AssertableJson $json) => $json->where('id', $nBookings + 1)
            ->where('price', $name)
            ->where('check_in_date', $age)
            ->where('check_out_date', $client->id)
            ->etc()
        );
    }

    /**
     * Booking data provider.
     *
     * @return Generator
     */
    public function bookingDataProvider(): Generator
    {
        for ($i = 0; $i < 1; $i++) {
            yield [
                $this->faker('en')->randomFloat(2),
                $this->faker('en')->dateTime("now"),
                $this->faker('en')->dateTime("now +1 day"),
            ];
        }
    }
}
